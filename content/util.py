import os

def construct_year_string(year):
    return str(int(year)-1) + '-' + str(year)[2:]

def write_pages():
    for year in range(1997,2019):
        year_string = construct_year_string(year)

        os.mkdir("season/{}".format(year_string))
        with open("season/{}/regular-season.md".format(year_string),"w") as mdfile:
            content = r"""---
title: year_string Regular Season RAPM
comments: false
date: april_first
---

<table id="rapm">
        <thead>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </tfoot>
    </table>

<script>
$(document).ready(function() {
    $('#rapm').DataTable( {
        "ajax": '../../../data/year_string-rapm.json',
        "pageLength": 25
    } );
} );
</script>"""
            content = content.replace("year_string", year_string)
            content = content.replace("april_first", "{}-04-01".format(year))
            mdfile.write(content)

        with open("season/{}/playoffs.md".format(year_string),"w") as mdfile:
            content = r"""---
title: year_string Playoffs RAPM
comments: false
date: july_first
---

<table id="rapm">
        <thead>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </tfoot>
    </table>

<script>
$(document).ready(function() {
    $('#rapm').DataTable( {
        "ajax": '../../../data/year_string-playoffs-rapm.json',
        "pageLength": 25
    } );
} );
</script>"""
            content = content.replace("year_string", year_string)
            content = content.replace("july_first", "{}-07-01".format(year))
            mdfile.write(content)
def write_home_page():
    content = ""
    for year in range(2018,1996,-1):
        year_string = construct_year_string(year)
        content += "- {}\n".format(year_string)
        content += "\t- [Regular Season](season/{}/regular-season)\n".format(year_string)
        content += "\t- [Playoffs](season/{}/playoffs)\n".format(year_string)
    with open("home.md", "w") as outfile:
        outfile.write(content)

if __name__ == "__main__":
    #write_pages()
    write_home_page()