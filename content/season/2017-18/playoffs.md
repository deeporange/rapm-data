---
title: 2017-18 Playoffs RAPM
comments: false
date: 2018-06-18
---

<table id="rapm">
        <thead>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </tfoot>
    </table>

<script>
$(document).ready(function() {
    $('#rapm').DataTable( {
        "ajax": '../../../data/2017-18-playoffs-rapm.json',
        "pageLength": 25
    } );
} );
</script>